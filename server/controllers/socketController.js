
const auth = require('./authController')
const path =require('path')
const user = require('./userController')
const fs = require('fs')
const server = require('./serverController')
const friendship= require('./friendshipController')
const srv = require('../../build/dev-server');
const io = srv.socket;

module.exports = {
  login: async (req, socket) => {
    //login
    var authorization = await auth.getAuth(req);
    if(authorization.body.auth){
    var userId = authorization.body.user.id;
    if (req.body.role == 'server') {
      registration = await server.register(req, userId);
      var serverId = registration[0].id;
      authorization.serverId=serverId;
      socket.join('s' + serverId)
      server.isConected({
        status: true,
        serverId: serverId
      });
      updateServerStatus(userId,registration[0],true)
      socket.on('disconnect', () => {
        server.isConected({
          status: false,
          serverId: serverId
        })
        updateServerStatus(userId,registration[0],false)
      })
    } else {
      socket.join('u' + userId);
      socket.join('users');
      user.isConected({
        status: true,
        userId: userId
      });
      socket.on('disconnect', () => {
        user.isConected({
          status: false,
          userId: userId
        })
      })
    }
  }
    socket.emit('authRes', authorization)
  },
  //register
  register: async (req, socket) => {
    var uncryptedPAssword = req.body.password //save to make login after registration
    var registration = await user.register(req); // return value is an array [0] user finded or created [1] true if a new object was created and false if not
    if (registration[1]) {
      var authorization = await auth.getAuth({
        body: {
          email: registration[0].email,
          password: uncryptedPAssword
        }
      }) //if registered get login
      //console.log(authorization);
      socket.emit('registerOk', authorization)
    } else
      (socket.emit('registerKo', 'User already exists'))
  },
  //logof event, a user makes log off
  logOff: async (data, socket) => {
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      socket.leave('u' + data.userId)
      socket.leave('users')
      user.isConected({
        status: false,
        userId: data.userId
      })
    }
  },

  keepAlive: async (data, socket) => {
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      socket.join('u' + data.userId);
      socket.join('users');
      user.isConected({
        status: true,
        userId: data.userId
      })
    }
  },

  reqServers: async (data,socket) =>{
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      var ownServers=await server.findByOwner(data.userId);
      var sharedServers=await user.reqAllowServers(data.userId)
      if(sharedServers){
        sharedServers=await sharedServers.servers.sort(function(a, b){return a.userId - b.userId})
        var servers=ownServers.concat(sharedServers)
      } else servers=ownServers
      //console.log(JSON.stringify(sharedServers))
      //console.log(JSON.stringify(servers))
      socket.emit('resServers',servers)
    }
  },

  reqFriend: async (data,socket)=> {
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      var friend=await user.findByEmail(data.email)
      if(friend){
        var friendshipReq=await friendship.requestFrienship(friend.id,data.user.id)
        if (friendshipReq[1]) io.to('u'+friend.id).emit('reqFriendship',{sender:data.user,createdAt:friendshipReq[0].createdAt})
      }else socket.emit('resFriend',{friend:data.email,created:false})
    }
  },
  resFriendship:async(data)=>{
    //console.log(data)
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      if(data.response==false) friendship.destroy(data.user.id,data.friend.id)
      else  {
        await friendship.requestFrienship(data.friend.id,data.user.id) //creatres frienship reciprocity
        friendship.updateStatus(data.user.id,data.friend.id) //update status to accepted:true
      }
      io.to('u'+data.friend.id).emit('resFriendship',{friend:data.user,accepted:data.response})
    }
  },

  reqNotifications:async(data,socket)=>{
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      var pendingNotifications= await user.getFriendships(data.userId,false);
      socket.emit('resNotifications',pendingNotifications.friend)
    }
  },
  reqFriends: async (data,socket)=>{
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      var myfriends= await user.getFriendships(data.userId,true);
      socket.emit('resFriends',myfriends.friend)
    }
  },
  reqSharedLibraries: async(data,socket)=>{
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify && data.friend.id) {
      sharedLibraries = await server.reqSharedLibraries(data.friend.id,data.server.id)
    }
    socket.emit('resSharedLibraries',sharedLibraries)
  },
  updateSharedLibraries:async(data,socket)=>{
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
     var libraries= await server.updateSharedLibraries({friendId:data.friend.id,serverId:data.server.id,add:data.add,library:data.library})
     data.server.allowed_server={libraries:libraries} //to limit libraries to allowed
    io.to('u'+data.friend.id).emit('updateSharedLibrary',{user:data.user,server:data.server,library:data.library,add:data.add})
    }
  },
  reqUpdateUser:async(data,socket)=>{
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      var result = user.updateUser(data.newPass,data.newName,data.newMail,data.user.id)
    }
    if(!result) socket.emit('resUpdateUser',data)
    else io.to('u'+data.user.id).emit('logOff')
  },
  reqEnFrienship:async(data)=>{
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      friendship.destroy(data.user.id,data.friend.id)
      server.disAllowServers(data.user.id)
      server.disAllowServers(data.friend.id)
    }
    io.to('u'+data.friend.id).emit('destroyFrienship',data.user)
  },

  reqUserImage:async(data,socket)=>{
      //console.log(data.user.id)
      try{
        var image=await fs.readFileSync(path.join('static/userImages/'+data.user.id+'.jpeg'))
        io.to('u'+data.user.id).emit('resUserImage',{image:image,userId:data.user.id})
        socket.emit('resUserImage',{image:image,userId:data.user.id})
      }catch(e){
        //console.log(e)
      }
      var friends=await friendship.reqAllFriends(data.user.id)
      friends.forEach(friend => {
        io.to('u'+friend.friendId).emit('resUserImage',{image:image,userId:data.user.id})
      })

  },
  uploadUserImage:async(data,socket)=>{
    var tokenVerify = await auth.tokenVerify(data.token);
    if (tokenVerify) {
      var ba64 = require("ba64"),
      data_url = "data:image/jpeg;base64,"+data.image;
        ba64.writeImage(path.join('static/userImages/')+data.user.id, data_url, async (err)=>{
        if (err) throw err;
        var image=await fs.readFileSync(path.join('static/userImages/'+data.user.id+'.jpeg'))
        io.to('u'+data.user.id).emit('resUserImage',{image:image,userId:data.user.id})
        socket.emit('resUserImage',{image:image,userId:data.user.id})
        var friends=await friendship.reqAllFriends(data.user.id)
        friends.forEach(friend => {
          io.to('u'+friend.friendId).emit('resUserImage',{image:image,userId:data.user.id})
        })
      });
    }
  },
  io: io,
}


async function updateServerStatus(userId,serv,status){
  io.to('u'+userId).emit('serverUpdateStatus',{server:serv,status:status});
  var usersToAlert=await server.usersAllowedAt(serv.id)
  usersToAlert.forEach(user=>{
    io.to('u'+user.userId).emit('serverUpdateStatus',{server:serv,status:status});
  })
}
