const Server = require('../models/Server')
const allowed_server = require('../models/Allowed_server')
const User = require('../models/User')
const user = require('../controllers/userController')
const Sequelize = require('sequelize');
const Op = Sequelize.Op


module.exports = {
  isConected: async (data) => {
    server = await Server.findByPk(data.serverId);
    server.update({
      isConected: data.status
    })

  },
  register: async (req, userId) => {
    req.body.userId = userId;
    return Server
      .findOrCreate({
        where: {
          [Op.and]: {
            userId: req.body.userId,
            name: req.body.name
          }
        },
        defaults: req.body
      })
  },

  findByOwner: async(userId)=>{
    return Server.findAll({
      where:{
        userId:userId
      }
    })
  },
  findById: async(serverId)=>{
    return Server.findOne({
      where:{
        id:serverId
      }
    })
  },

  reqSharedLibraries:async (userId,serverId)=>{
  var server=await allowed_server.findOne({
    where:{
      userId,
      serverId
    },
  })
  if(server) return server.libraries
},

updateSharedLibraries:async(data)=>{
  var libraries = await updateSharedLibraries(data)
  return libraries
},

deleteAllSharesOf: async (library,serverId)=>{
  sharesToDelete = await allowed_server.findAll({where:{serverId}})
  await sharesToDelete.forEach(share => {
    updateSharedLibraries({friendId:share.userId,serverId:serverId,library:library})
  });
  return sharesToDelete
},

disAllowServers:async (userId)=>{
  allowed=await user.reqAllowServers(userId)
  allowed=allowed.servers
  allowed.forEach(async element => {
    var line=await allowed_server.findAll({where:{serverId:element.id,userId:element.allowed_server.userId}})
    line.forEach(e=>{
      e.destroy()
    })
  });
},
usersAllowedAt:(serverId)=>{
  return allowed_server.findAll({where:{serverId:serverId}})
}

}

async function updateSharedLibraries(data){
 var serverId=data.serverId
 var friendId=data.friendId

    var  server = await allowed_server.findOrCreate({
      where:{
        serverId:serverId,
        userId:friendId
      }})
    if(data.add){
      if(server[0].libraries==null)server[0].libraries=JSON.stringify([data.library._id])
      else {
        var libs=JSON.parse(server[0].libraries)
        libs.push(data.library._id)
        server[0].libraries=JSON.stringify(libs)
      }
      server[0].save()
    }
    else {
      var libs=JSON.parse(server[0].libraries)
      libs.splice(libs.indexOf(data.library._id),1)
      if (libs.length==0) server[0].destroy() //if no sared libraries auto loss server access
      else {
        server[0].libraries=JSON.stringify(libs)
        server[0].save()
      }
    }
    return server[0].libraries

}
