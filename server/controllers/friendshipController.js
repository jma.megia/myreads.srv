var Friend=require('../models/Friend')
const Sequelize = require('sequelize');
const Op = Sequelize.Op

async function getByPk(pk1,pk2){
  var frienship=await Friend.findAll({where:{
    [Op.or]:[
    {
      userId:pk1,
      friendId:pk2
    },{
      userId:pk2,
      friendId:pk1
    }
  ]
  }})
  return frienship;
}


module.exports={
  async reqAllFriends(userId){
    return Friend.findAll({where:{userId:userId}})
  },

  async requestFrienship(userId,friendId){
    var friendshipRequest = await Friend.findOrCreate({
      where:{
        userId:userId,
        friendId:friendId
      },
      defaults:{accepted:false},
    })
    return friendshipRequest
  },

  async destroy(userId,friendId){
    var res = await getByPk(userId,friendId)
    res.forEach(element => {
      element.destroy()
    });
  },

  async updateStatus(userId,friendId){
    var res = await getByPk(userId,friendId)
    res.forEach(element => {
      element.accepted=true;
      element.save()
    });
  }
}
