var Server =require('../models/Server')
var User = require('../models/User')
const bcrypt = require('bcrypt');
const Sequelize = require('sequelize');
const Op = Sequelize.Op
module.exports = {
  isConected: async (data) => {
    //console.log(data)
    user = await User.findByPk(data.userId);
    user.update({
      isConected: data.status
    })

  },
  findByEmail: async (email) => {
    return User
      .findOne({
        attributes:['id','name','email','isConected','password'],
        where: {
          email: email
        }
      })
  },
  findByStatus: async (status) => {
    return User.findAll({
      attributes:['id','name','email','isConected'],
      where: {
        isConected: status
      }
    })
  },

  register: async (req) => {
    req.body.password = bcrypt.hashSync(req.body.password, 8), 1;
    return User
      .findOrCreate({
        attributes:['id','name','email','isConected'],
        where: {
          [Op.or]: {
            email: req.body.email,
            name: req.body.name
          }
        },
        defaults: req.body
      })
  },
  async findBypk(pk){
    user=await User.findByPk(pk,{attributes:['id','name','email','isConected']})
    return user
  },

  updateUser : async(newPass,newName,newMail,userId)=>{
    try{
      user=await User.findByPk(userId)
      if(newName)user.name=newName
      else if(newMail)user.email=newMail
      else if(newPass) user.password= bcrypt.hashSync(newPass, 8), 1
      user.save()
      return true
    }catch{return false}
  },
  getFriendships:async (userId,status)=>{
    var user=await User.findByPk(userId,{
      include:[{
        attributes:{exclude:['password','isAdmin']},
        association : 'friend',
        through: {
          attributes:[['createdAt', 'accepted']],
          where:{accepted:status}
        },
      },
      ],
    })
   // console.log('My : ' + ' id :'+user.id+ ' name ' +user.name +' Friend : '  + ' id :'+user.friend[0].id+ ' name '+ user.friend[0].name  + ' = > '+user.friend[0].friendship.createdAt )
   // /console.log(user.friend[0].friendship.accepted)
    return user
  },
  reqAllowServers: async(userId)=>{
    return User.findOne({
          include:[{
            model:Server,
            through:{
              attributes:['userId','libraries'],
              where:{
                userId:userId
              }
            },
          }],
          where:{
            id:userId
          }
    })
  },
}
