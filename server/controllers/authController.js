const User = require('./../controllers/userController');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config');

module.exports = {
  getAuth: async (req) => {
    var user = await User.findByEmail(req.body.email);
    if (user) {
      var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
      if (passwordIsValid) {
        let token = jwt.sign({
          id: user.id
        }, config.secret, {
          expiresIn: 86400
        }); // expires in 24 hours
        return {
          status: 200,
          body: {
            auth: true,
            token: token,
            user: {id:user.id,name:user.name,email:user.email,isConected:user.isConected},
            is_admin: user.is_admin
          }
        }
      } else return {
        body: {
          auth: false
        }
      }
    } else
      return {
        body: {
          auth: false
        }
      }
  },

  tokenVerify: (token) => {
    return jwt.verify(token, config.secret, (err, decoded) => {
      if (err) return false
      else return true
    });
  }
}
