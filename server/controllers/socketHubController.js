const auth = require('./authController')
const server = require('./serverController')
const srv = require('../../build/dev-server')
const friendship= require('./friendshipController')
const io = srv.socket;

 module.exports={
   //ask for books to server
 reqBooks: async (data, socket) => {
  var tokenVerify = await auth.tokenVerify(data.token);
  userId = data.id;
  serverId=data.serverId;
  if (tokenVerify) {
    //console.log('askForBooks')
    io
      .to('s' + serverId)
      .emit('reqBooks', {socketId:socket.id, library:data.library,userId:userId});
  }
},
resBooks: async (data) => {
  userId = data.userId
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
   // console.log('sending books to user: ' + userId + ' from server: '+data.serverId)
    io
      .to(data.socketId)
      .emit('resBooks server '+data.serverId, {books:data.books,reads:data.reads});
  }
},
reqBook: async (data,socket) => {
 // console.log('requering book to server:' + data.serverId)
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    data.socketId=socket.id
    io.to('s' + data.serverId).emit('reqBook', data)
  }
},
resBook: async (data) => {
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    var evnt=''
    if(data.download) evnt='resDownloadBook'
    else evnt='resBook'
    //console.log(evnt+ ' => ' + data.download)
    io.to(data.socketId).emit(evnt, {
      book: data.book,
      progress:data.progress,
      fileName:data.fileName
    })
  }
},

newBook:async (data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    var serv=await server.findById(data.serverId);
    io.to('u'+serv.userId).emit('newBook server '+data.serverId,data);
    var usersToAlert=await server.usersAllowedAt(data.serverId)
    usersToAlert.forEach(user=>{
      if(user.libraries.includes(data.libraries[0]._id))io.to('u'+user.userId).emit('newBook server '+data.serverId,data);
    })
    //console.log('new book send to '+serv.userId)
  }
},

deletedBook:async(data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    var serv=await server.findById(data.serverId);
    io.to('u'+serv.userId).emit('deletedBook server '+data.serverId,data);
    var usersToAlert=await server.usersAllowedAt(data.serverId)
    usersToAlert.forEach(user=>{
      if(user.libraries.includes(data.libraries[0]._id))io.to('u'+user.userId).emit('deletedBook server '+data.serverId,data);
    })
    //console.log('deletedBook send to '+serv.userId)
  }
},

reqFiles:async(data,socket)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  //console.log('requering files ' + data.serverId)
  if (tokenVerify) {
    data.socketId=socket.id
    io.to('s'+data.serverId).emit('reqFiles',data)
  }
},

resFiles:async(data)=>{
  //console.log('responsing files')
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to(data.socketId).emit('resFiles',data)
  }
},


resNewLibrary:async(data)=>{
  //console.log('new library to ' +data.userId)
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('u'+data.userId).emit('resNewLibrary',data)
  }
},
reqNewLibrary:async(data)=>{

  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('s'+data.serverId).emit('reqNewLibrary',data)
    //console.log('creating library at server: '+ data.serverId)
  }
},
resDelLibrary:async(data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('u'+data.userId).emit('resDelLibrary',data)
    //deleting shares of this library
    if(data.library){
      var shares=await server.deleteAllSharesOf(data.library,data.serverId)
      shares.forEach(element => {
        io.to('u'+element.userId).emit('resDelLibrary',data)
    });
    }
  }
},
reqDelLib:async(data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('s'+data.serverId).emit('reqDelLibrary',data)
  }
},


resRenameLibrary:async(data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('u'+data.userId).emit('resRenameLibrary',data)
  }
},

reqRenameLibrary:async(data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('s'+data.serverId).emit('reqRenameLibrary',data)
  }
},

reqLibraries:async (data,socket)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    data.socketId=socket.id
    io.to('s'+data.server.id).emit('reqLibraries',data)
  }
},

resLibraries:async (data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to(data.socketId).emit('resLibraries',data.libraries)
  }
},
reqBookComment: async (data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);

  if (tokenVerify) {
    io.to('s'+data.serverId).emit('reqBookComment',data)
  }
},

resBookComment:async (data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('u'+data.userId).emit('resBookComment',data.confirmation)
  }
  var friends=await friendship.reqAllFriends(data.userId)
  friends.forEach(friend => {
    io.to('u'+friend.friendId).emit('resBookComment',data.confirmation)
  })
},
reqComments: async (data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);

  if (tokenVerify) {
    io.to('s'+data.serverId).emit('reqComments',data)
  }
},

resComments:async (data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('u'+data.userId).emit('resComments',data.comments)

  }
},

reqSetReaded: async (data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('s'+data.serverId).emit('reqSetReaded',data)
  }
},

reqDeleteValoration:async (data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('s'+data.serverId).emit('reqDeleteValoration',data)
  }
},

resDeleteValoration:async (data)=>{
  var tokenVerify = await auth.tokenVerify(data.token);
  if (tokenVerify) {
    io.to('u'+data.userId).emit('resDeleteValoration',data.confirmation)
    var friends=await friendship.reqAllFriends(data.userId)
    friends.forEach(friend => {
      io.to('u'+friend.friendId).emit('resDeleteValoration',data.confirmation)
    })
  }
},

io: io

 }
