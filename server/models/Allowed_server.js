
const Sequelize = require('sequelize');
const sequelize = require('../db/db');
const Allowed_server = sequelize.define('allowed_server', {
  libraries: {
    type: Sequelize.STRING(2000),
    allowNull : true,
  },
});
sequelize.sync()
module.exports = Allowed_server;
