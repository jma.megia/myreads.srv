const Sequelize = require('sequelize');
const sequelize = require('../db/db');
const Friendship = sequelize.define('friendship', {
  accepted: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
});
sequelize.sync()
module.exports = Friendship;
