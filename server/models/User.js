const Sequelize = require('sequelize');
const sequelize = require('../db/db');
const User = sequelize.define('user', {
  name: Sequelize.STRING,
  email: Sequelize.STRING,
  password: Sequelize.STRING,
  isConected: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  is_admin: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
});
module.exports = User;

 //relations
 const Server = require('./Server')
 const Allowed_server=require('./Allowed_server')
 const Friendship=require('./Friend')


 User.hasMany(Server, {
   as: 'myservers'
 });
User.belongsToMany(User,{
  as:'friend',
  through:Friendship
})

 User.belongsToMany(Server,{
   through:Allowed_server,
 })

 sequelize.sync()
