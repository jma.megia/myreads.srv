const Sequelize = require('sequelize');
const sequelize = require('../db/db');
const Server = sequelize.define('server', {
  name: Sequelize.STRING,
  isConected: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
});
sequelize.sync()
module.exports = Server;

//relations
const User = require('./User')
const Allowed_server = require('./Allowed_server.js')
Server.belongsToMany(User,{
  through:Allowed_server,
})


