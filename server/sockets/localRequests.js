const socketContoller = require('../controllers/socketController')
socketContoller
  .io
  .on('connection', (socket) => {
    socket.on('login', (req) => {
      socketContoller.login(req, socket);
    })

    socket.on('reqServers',(req) =>{
      socketContoller.reqServers(req, socket)
    })

    socket.on('register', (req) => {
      socketContoller.register(req, socket);
    })

    socket.on('logOff', (req) => {
      socketContoller.logOff(req, socket)
    })

    socket.on('keepAlive', (req) => {
      socketContoller.keepAlive(req, socket)
    })
    socket.on('reqFriend',(req)=>{
      socketContoller.reqFriend(req,socket)
    })

    socket.on('resFriendship',(req)=>{
      socketContoller.resFriendship(req)
    })
    socket.on('reqNotifications',(req)=>{
      socketContoller.reqNotifications(req,socket)
    })
    socket.on('reqFriends',(req)=>{
      socketContoller.reqFriends(req,socket)
    })
    socket.on('reqSharedLibraries',(req)=>{
      socketContoller.reqSharedLibraries(req,socket)
    })
    socket.on('updateSharedLibraries',(req)=>{
      socketContoller.updateSharedLibraries(req,socket)
    })

    socket.on('reqUpdateUser',(req)=>{
      socketContoller.reqUpdateUser(req,socket)
    })

    socket.on('uploadUserImage',(req)=>{
      socketContoller.uploadUserImage(req,socket)
    })

    socket.on('reqUserImage',(req)=>{
      socketContoller.reqUserImage(req,socket)
    })
    socket.on('reqEnFrienship',(req)=>{
      socketContoller.reqEnFrienship(req,socket)
    })

  })
