const socketHub = require('../controllers/socketHubController')
socketHub.io
.on('connection', (socket) => {
  socket.on('reqBooks', (req) => {
    socketHub.reqBooks(req,socket)
  })

  socket.on('resBooks', (req) => {
    socketHub.resBooks(req)
  })
  socket.on('reqBook', (req) => {
    socketHub.reqBook(req,socket)
  })

  socket.on('resBook', (res) => {
    socketHub.resBook(res)
  })

  socket.on('newBook',(req)=>{
    socketHub.newBook(req)
  })

  socket.on('bookDeleted',(req)=>{
    socketHub.deletedBook(req)
  })

  socket.on('resFiles',(res)=>{
    socketHub.resFiles(res)
  })

  socket.on('reqFiles',(req)=>{
    socketHub.reqFiles(req,socket)
  })

  socket.on('reqNewLibrary',(res)=>{
    socketHub.reqNewLibrary(res)
  })

  socket.on('reqLibraries',(req)=>{
    socketHub.reqLibraries(req,socket)
  })

  socket.on('resLibraries',(req)=>{
    socketHub.resLibraries(req)
  })

  socket.on('resNewLibrary',(res)=>{
    socketHub.resNewLibrary(res)
  })

  socket.on('reqDelLibrary',(req)=>{
    socketHub.reqDelLib(req)
  })

  socket.on('resDelLibrary',(res)=>{
    socketHub.resDelLibrary(res)
  })
  socket.on('reqRenameLibrary',(req)=>{
    socketHub.reqRenameLibrary(req)
  })

  socket.on('resRenameLibrary',(res)=>{
    socketHub.resRenameLibrary(res)
  })
  socket.on('reqBookComment',(req)=>{
    socketHub.reqBookComment(req)
  })
  socket.on('resBookComment',(res)=>{
    socketHub.resBookComment(res)
  })
  socket.on('reqComments',(req)=>{
    socketHub.reqComments(req)
  })
  socket.on('resComments',(res)=>{
    socketHub.resComments(res)
  })
  socket.on('reqSetReaded',(req)=>{
    socketHub.reqSetReaded(req)
  })
  socket.on('reqDeleteValoration',(req)=>{
    socketHub.reqDeleteValoration(req)
  })
  socket.on('resDeleteValoration',(res)=>{
    socketHub.resDeleteValoration(res)
  })

})
