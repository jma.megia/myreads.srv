// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import bootstrap from "bootstrap"
import App from './App'
import router from './routes'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
import io from 'socket.io-client';
import VModal from 'vue-js-modal'

const config=require('../config')
window.toastr = require('toastr')




import {
  FontAwesomeIcon,
  FontAwesomeLayers,
  FontAwesomeLayersText
} from '@fortawesome/vue-fontawesome'
import {
  library,dom
} from '@fortawesome/fontawesome-svg-core'
dom.watch();
import { faUserCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import {
  faFolder,
  faSortAmountUp,
  faSortAmountDown,
  faUser,
  faCoffee,
  faSquare,
  faSignInAlt,
  faEdit,
  faHome,
  faUserEdit,
  faSignOutAlt,
  faStar,
  faSortUp,
  faSortDown,
  faDownload,
  faBookReader,
  faEye,
  faBook,
  faPenFancy,
  faSearch,
  faTimes,
  faPlus,
  faTrashAlt,
  faFile,
  faReply,
  faCommentAlt,
  faCheck,
  faExternalLinkSquareAlt,
  faBookDead,
  faSwatchbook,
  faServer

} from '@fortawesome/free-solid-svg-icons'

library.add(
  faCoffee, faUser, faSquare, faSignInAlt, faEdit, faUserEdit, faSignOutAlt, faHome, faStar, faSortUp, faSortDown, faDownload,
  faBookReader, faEye, faSortAmountDown, faSortAmountUp, faBook, faPenFancy, faSearch, faTimes, faFolder,faPlus,faTrashAlt,faFile,
  faReply,faUserCircle, faCheckCircle, faCommentAlt,faCheck,faExternalLinkSquareAlt,faBookDead,faSwatchbook,faServer
)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.component('font-awesome-layers-text', FontAwesomeLayersText)

//global
const EventBus = new Vue();
export default EventBus;

const socket = io(config.dev.HOST+':'+config.dev.PORT);

VueToastr2.defaultPosition="toast-bottom-right"
Vue.use(VModal)
Vue.use(VueToastr2)
Vue.use(BootstrapVue)
Vue.config.productionTip = false
Vue.prototype.$socket = socket;
/* eslint-disable no-new */

//if session expires
socket.on('endSession', () => {
  this.$router.push('login')
  this.$toastr.error('Session expired', 'Please login again', {
    preventDuplicates: true,
    progressBar: true
  });
})


var vm = new Vue({
  data() {
    return {

    }
  },

  router,
  components: {
    App
  },
  template: '<App/>',

}).$mount('#app')
