import Vue from 'vue'
import Router from 'vue-router'
import Ind from '@/components/Index'
import Login from '@/components/auth/Login'
import Register from '@/components/auth/Register'
import UserBoard from '@/components/user_board/UserBoard'
import Admin from '@/components/user_board/Admin'
import UserProfile from '@/components/user_profile/UserProfile'


Vue.use(Router)
let router = new Router({
  mode: 'history',
  routes: [{
      path: '*',
      name: 'redirectTologin',
      component: Login,
      meta: {
        guest: true
      }
    },

    {
      path: '/',
      name: 'index',
      component: Ind
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        guest: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        guest: true
      }
    },
    {
      path: '/dashboard/:name',
      name: 'home/user',
      component: UserBoard,

      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/profile/:name',
      name: 'profile/user',
      component: UserProfile,

      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin,
      meta: {
        requiresAuth: true,
        is_admin: true
      }
    },
  ]
})

router.beforeEach((to, from, next) => {
  let user = JSON.parse(localStorage.getItem('user'));
  let auth = localStorage.getItem('jwt');


  // if auth is required
  if (to.matched.some(record => record.meta.requiresAuth)) {
    //if is not auth
    if (localStorage.getItem('jwt') == null) {
      next({
        path: '/login',
        params: {
          nextUrl: to.fullPath
        }
      })
      //is ath
    } else {
      let user = JSON.parse(localStorage.getItem('user'))
      //admin is required
      if (to.matched.some(record => record.meta.is_admin)) {
        //is admin
        if (user.is_admin == 1) {
          next()
        }
        //is not admin
        else {
          next({
            name: 'home/user'
          })
        }
        // no admin requiered (yes require auth)
      } else {
        next()
      }
    }
    //if not auth required
  } else if (to.matched.some(record => record.meta.guest)) {
    // if not auth -> go
    if (localStorage.getItem('jwt') == null) {
      next()
    } else {
      // if auth -> go userboard
      next({
        name: 'home/user'
      })
    }
  } else {
    next()
  }
})

export default router
