var fs = require('fs');
var https = require('https');
var http=require('http')
var express = require('express');
const PORT = 443;
var serveStatic = require('serve-static');

var app = express();
var appSec=express();
var server =http.createServer(app).listen(80,()=>{
  console.log('listen on port http')
  require('./server/sockets');
})
const socket = require('socket.io')(server)


sslServer=https.createServer({
    key: fs.readFileSync('/etc/letsencrypt/live/myreads.tk/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/myreads.tk/cert.pem')
}, appSec).listen(PORT, function(){
    console.log("My https server listening on port " + PORT + "...");
});
const sslSsocket = require('socket.io')(sslServer)

//open static dir (files generated by webpack)
appSec.use(serveStatic(__dirname + "/dist/"));
appSec.use('/static', express.static(__dirname + "/dist/static"));


//routes
app.get('*',(req,res)=>{
  res.redirect('https://' + req.headers.host + req.url);
})

module.exports={
  socket:sslSsocket
}
